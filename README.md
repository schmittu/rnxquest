# RNxQuest
A Python package to support analysis of protein-RNA cross-linking data with xQuest. 

In order to use RNxQuest, the software xQuest V2.1.5 must be installed. More details [can be found here](https://gitlab.ethz.ch/leitner_lab/xquest_xprophet/-/wikis/Home/Installation). In theory, both xQuest and RNxQuest are operating system agnostic, however in practice, both have been most extensively used and tested in Unix-like environments (i.e. Ubuntu or a Unix-based cluster computing environment).

## Documentation
Extensive documentation is provided in the Wiki associated with this repository ([available here](https://gitlab.ethz.ch/leitner_lab/rnxquest/-/wikis/home)). For most users, it is strongly reccomended to consult this before continuing. Brief instructions are provided below which may help advanced users get srated more quickly. 

## Using the RNxQuest/xQuest Dockerfile

The easiest way for most users to set up and run RNxQuest is via the RNxQuest Dockerfile. The commonly used Docker virtualisation approach configures a container with the correct environment setup to run both conventional protein-protein XL-MS xQuest analysis with xQuest, and protein-RNA XL-MS data analysis with RNxQuest and xQuest together. More information [can be found here](https://gitlab.ethz.ch/leitner_lab/rnxquest/-/wikis/Installing-and-using-the-RNxQuest-Docker-container). 

## Installation after cloning the Gitlab repository

The package is not currently available on a public repository. However, it conforms to the standards of the [Python Package Index (PyPI)](https://pypi.org/). The repository can be cloned ([help with this](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)) with the following command:

`git clone https://gitlab.ethz.ch/leitner_lab/rnxquest.git`

The package can then be installed locally using:

`pip install -e <path_to_clone>\rnxquest\`

Or if using the ETH Euler cluster:

`pip install -e <path_to_clone>\rnxquest\ --user`

In the above example, <path_to_clone> should be exchanged for the path to the directory where the `git clone ...` command above was executed. 

## Direct installation from the Gitlab repository with pip

The Gitlab repository is formatted such that the package can be installed directly from pip with the following command:

`pip install git+https://gitlab.ethz.ch/leitner_lab/rnxquest.git`


