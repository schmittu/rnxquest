#!/usr/bin/env python
# coding: utf-8

# v0.21
# - reformats as part of a python package
# v0.2
# - Fixes bug in column name; delivers RNA loss type to "loss_type" instead of "type_y". Must be used with Gen_xQ_params v0.86 or greater. 

# # xQuest data extraction
# Imports data from multiple xquest.xml files. It requires:
#         
# Please install required packages e.g. numpy, pandas, biopython, plotly_express from the anaconda console
# 

from lxml import etree as ET
import os
import csv
import re
import argparse
import pandas as pd
import warnings
warnings.filterwarnings('ignore')

def load_fasta(file_name, preference='id'):
    name = None
    seqs = dict()
    s = ''
    settings = [">..\|[A-Z0-9]+\|([A-Z0-9_]+)\s.+", ">.+GN=(.+?)\s.+", ">..\|[A-Z0-9]+\|[A-Z0-9_]+\s(.+)\sOS.+", ">..\|([A-Z0-9]+)\|.+", "(.+)"]
    with open(file_name,'r') as file:
        filedata = file.readlines()
        for line in filedata:
            line = line.rstrip()
            if line.startswith('>'):
                if name:
                    seqs[name]=s
                if(preference == 'gn'):
                    filter = settings[1]
                elif(preference == 'name'):
                    filter = settings[2]
                elif(preference == 'uid'):
                    filter = settings[3]
                elif(preference == 'full'):
                    filter = settings[4]
                else:
                    filter = settings[0]
                
                name = re.sub(filter,r'\1', line)
                if preference != 'full' and line == name: # when filtering didn't work, try all filters and use full header otherwise
                    for filter in settings:
                        name = re.sub(filter,r'\1', line)
                        if name != line:
                            break
                seqs[name] = ''
                s = ''
            else:
                s = s + line
        if name: #just checking against an empty file
           seqs[name]=s
    return seqs

def main():
    # ## Read command line arguments
    parser = argparse.ArgumentParser(
        description='A script to extract a consolidated csv file from xquest.xml files of one analysis.')
    parser.add_argument('-list', '--list', required=True,
                        help='path to csv-file containing mzXML and experiment name')
    parser.add_argument('-fasta', '--fasta_file', required=True,
                        help='path to fasta-file used for analysis')
    parser.add_argument('-exp', '--experiment', required=True,
                        help='file name pattern for experiments')
    parser.add_argument('-filter_rank', '--filter_rank', required=False, default = "1",
                        help='filter for search_hit_rank = 1 (1 for true[default], 0 for false)')
    parser.add_argument('-minscore', '--minimum_score', required=False, default = "0",
                        help='Minimum cross-link during import of data (default 0)')
    parser.add_argument('-seqshift', '--sequence_shift', required=False, default = "0",
                        help='Shift of wildtype sequence to sequence in fasta database (default 0)')
    parser.add_argument('-varmod', '--variable_modification', required=False, default = "M",
                        help='Amino acid code that is a variable modification in xquest (default M)')
    parser.add_argument('-fasta_header', '--fasta_header', required=False, default = "id",
                        help='Select way to represent fastas in output. options: id - protein id, uid - uniprot-id, gn - gene name, full - complete fasta header (default: id)')

    args = parser.parse_args()

    # ## Setup
    # 
    # Please change the required parameters here:

    #path to csv-file containing mzXML and experiment name
    association_file = args.list #e.g. "//nfs/nas22.ethz.ch/fs2202/biol_imsb_aebersold_1/mgoetze/xq/mzXML/reanalysis_TDP43.csv"
    # path to fasta file used for searching
    fasta_file = args.fasta_file
    # Regex Pattern of all Searches that should be combined equals 'RESULT' from runxqmulti.sh
    file_pattern = args.experiment# e.g. "Reanalysis_TDP43"
    # output file name (Can remain static)
    out_file = "extracted.xQuest.csv"
    # variable modified amino acid residue in xQuest (could be parsed from xQuest.def)
    var_mod = "M"
    # If true, only search_hit_rank==1 hits will be saved, which is faster and uses less space (optional argument)
    filter_by_rank = args.filter_rank =="1"
    # Score prefilter: minimum score to include (already at the stage of importing from xQuest.xml; optional argument)
    minimum_score = int(args.minimum_score)
    # Shift of wildtype sequence to sequence in fasta database (default 0)
    sequence_shift = int(args.sequence_shift)
    # way to represent fastas in output. options: id - protein id, uid - uniprot-id, gn - gene name, full - complete fasta header (default: id)
    fasta_preference = args.fasta_header

    # ## Determine subfolders

    # All subfolders matching the pattern are determined. Check whether all subfolders are included.

    #Compile the Regex pattern for the input files and read directly from the search folder (not the xquestresultfolder)
    selected_directories = []
    shifts = re.compile("Shift.*")
    regex = re.compile(file_pattern+".+")

    for shift in list(filter(shifts.search,os.listdir())):
        # create list of subfolders to search and output the list for debugging purposes
        
        selected_directories += [shift+"/" + s for s in list(filter(regex.search,os.listdir(shift+"/")))]

    # ## Extract data from all _xquest.xml_ files
    # Data from all xquest.xml files is collected and stored directly in a csv file. Data for each hit includes all <i>spectrum_search</i> attributes as well as all <i>search_hit</i> attributes. At this stage data will already be filtered for **search_hit_rank** and **score**

    # List all keys (columns), that should be exported to the csv file from the spectrum_search nodes
    spec_keys = ['Mr_precursor', 
                 'addedMass', 
                 'apriori_pmatch_common', 
                 'apriori_pmatch_xlink', 
                 'charge_precursor', 
                 'ionintensity_stdev', 
                 'iontag_ncandidates', 
                 'mean_ionintensity', 
                 'mz_precursor', 
                 'mzscans', 
                 'ncommonions', 
                 'nxlinkions', 
                 'rtsecscans', 
                 'scantype', 
                 'spectrum']
    # List all keys (columns), that should be exported to the csv file from the search_hit nodes
    hit_keys = ['Mr', 
                'TIC', 
                'TIC_alpha', 
                'TIC_beta', 
                'annotated_spec', 
                'apriori_match_probs', 
                'apriori_match_probs_log', 
                'backboneions_matched', 
                'charge', 
                'error', 
                'error_rel', 
                'id', 
                'intsum', 
                'match_error_mean', 
                'match_error_stdev', 
                'match_odds', 
                'match_odds_alphacommon', 
                'match_odds_alphaxlink', 
                'match_odds_betacommon', 
                'match_odds_betaxlink',
                'measured_mass', 
                'mz', 
                'num_of_matched_common_ions_alpha', 
                'num_of_matched_common_ions_beta', 
                'num_of_matched_ions_alpha', 
                'num_of_matched_ions_beta', 
                'num_of_matched_xlink_ions_alpha', 
                'num_of_matched_xlink_ions_beta', 
                'prescore', 
                'prescore_alpha', 
                'prescore_beta', 
                'prot1', 
                'prot2', 
                'score', 
                'search_hit_rank', 
                'seq1', 
                'seq2', 
                'series_score_mean', 
                'structure', 
                'topology', 
                'type', 
                'wTIC', 
                'weighted_matchodds_mean', 
                'weighted_matchodds_sum', 
                'xcorrall', 
                'xcorrb', 
                'xcorrx', 
                'xlinkermass', 
                'xlinkions_matched', 
                'xlinkposition']

    # write header containing all keys to out_file
    header = spec_keys + hit_keys + ['Search']
    data = []

    # iterate over all xQuest.xml files of all selected directories to combine in one output file
    for i, directory in enumerate(selected_directories):
        in_file = directory + '/merged_xquest.xml'
        print("File "+ str(i + 1) + " of " + str(len(selected_directories)) + " " + in_file)
        search = directory.replace(file_pattern,"")
        # open the xquest.xml file (in_file)
        doc = ET.parse(in_file)
        
        # iterate over all nodes of spectrum_search (might not contain a hit!)
        for spectrum in doc.iterfind('//spectrum_search'):
            # iterate over all children of this node if there are no children, loop is not entered
            for hit in spectrum:
                # filter by search_hit_rank unless filter_by_rank is false
                if hit.get('search_hit_rank')=='1' or not filter_by_rank:
                    if minimum_score <= 0 or float(hit.get('score')) > minimum_score:
                        # load all values for the selected keys into the list data
                        entry = []
                        for key in spec_keys:
                            entry.append(spectrum.get(key))
                        for key in hit_keys:
                            entry.append(hit.get(key))
                        # write entry as csv-string to output file
                        entry.append(search)
                        data.append(entry)
                    
    print('done reading mzXML files')

    print('unmodify peptides from "X" to ' + var_mod)
    # load output file as pandas data frame
    df = pd.DataFrame(data,columns=header)
    # unmodify peptides by reversing X to unmodified amino acid e.g. 'M'
    df['unmod_pep'] = df['seq1'].replace("X",var_mod, regex = True).replace("([^A-Z])+", "", regex = True)

    # create empty columns for pep_start, pep_end, AbsPos1, the matched fasta_entry and alternative cross-link positions regarding the used fasta file!
    df['pep_start'] = 'NA'
    df['pep_end'] = 'NA'
    df['AbsPos1'] = -1
    df['AA'] = ''
    df['fasta_entry'] = 'NA'
    df['alternative_position'] = ''

    #Search through the fasta file to assign cross-link positions
    proteins = load_fasta(fasta_file, preference = fasta_preference)


    df_size = df.shape[0]
    fasta_size = len(proteins)
    progress = 0
    j = 0

    print('Assigning Absolute positions of cross-links')
    for protein_id in proteins.keys():
        progress = round(j * 20 / fasta_size)
        sequence = str(proteins[protein_id])
        for i, row in df.iterrows():
            p = round((j + i/df_size) * 20 / fasta_size)
            if p>progress:
                progress = p
                print(str(progress * 5) + '%')
            start = sequence.find(row['unmod_pep']) + sequence_shift
            if start >= 0:
                abspos = start + int(row['xlinkposition'])
                aa = sequence[abspos - sequence_shift - 1]
                if row['fasta_entry'] == 'NA':
                    df.loc[df.index[i], 'pep_start'] = start + 1
                    df.loc[df.index[i], 'pep_end'] = start + len(row['seq1'])
                    df.loc[df.index[i], 'AbsPos1'] = abspos
                    df.loc[df.index[i], 'AA'] = aa
                    df.loc[df.index[i], 'fasta_entry'] = protein_id
                    # Get the previous and next AAs in the sequence for mokapot
                    try:
                        df.loc[df.index[i], 'aa_bef'] = sequence[start-1]
                    except IndexError:
                        df.loc[df.index[i], 'aa_bef'] = "_"
                    try:
                        df.loc[df.index[i], 'aa_aft'] = sequence[start + len(row['seq1'])]
                    except IndexError:
                        df.loc[df.index[i], 'aa_aft'] = "_"
                else:
                    entry = "(" + protein_id + "-" + aa + str(abspos) + ")"
                    df.loc[df.index[i], 'alternative_position'] += entry

        j += 1
    print("saving file")
    df.to_csv(out_file)
    print('done')

    print('associating experiments to spectrum files')
    # read file association table
    file_df = pd.read_csv(association_file, names=['filename','experiment'])
    # for compatibility with resuldirectories file:
    file_df['filename'] = file_df['filename'].str.replace("_matched","")
    data_df = df
    # extract filenames from the respective columns in the xquest result and the file_associations.csv
    file_df['filename'] = file_df['filename'].str.split("[./]", n = 1, expand = True)[0] 
    data_df['filename'] = data_df['spectrum'].str.split("[./]", n = 1, expand = True)[0] 
    # merge the two data frames by the column 'filename'
    merge_res = pd.merge(data_df, file_df, on='filename', how='inner')


    print("associating nucleotide adducts to cross-linker mass")
    # Add information on nucleotide adduct types
    # load the file containing all mass shifts (__CLIR_mass_shifts.csv__) and combine equal masses

    # load CLIR_mass_shifts.csv into DataFrame
    CLIR_mass_shifts = pd.read_csv('CLIR_MS_mass_shifts_for_R.csv')

    # Round the masses to the 3rd digit to ensure proper matching later
    CLIR_mass_shifts['mass'] = round(CLIR_mass_shifts['mass'], 3)

    # remove all entries that list the Shift of the nucleotide combinations
    CLIR_mass_shifts =  CLIR_mass_shifts[CLIR_mass_shifts['loss_type'] != 'Shift']

    # compile a column with combined modification type e.g. GU Natural_-H2O
    CLIR_mass_shifts['mod_type'] = CLIR_mass_shifts['modification'] + " " + CLIR_mass_shifts['loss_type']

    # Remove duplicate masses from the list
    shifts = CLIR_mass_shifts.drop_duplicates(subset='mass', keep='first')
    shifts['all_possible_modtypes']= ''

    # compile a list of all possible nucleotide adducts that match to the rounded mass. and save to new column
    for i, row in shifts.iterrows():
        test = "/".join(str(x) for x in list(CLIR_mass_shifts[CLIR_mass_shifts.mass == row['mass']]['mod_type']))
        shifts.loc[i, 'all_possible_modtypes'] = test

    # remove the temporary mod_type column again
    shifts = shifts.drop(columns='mod_type')

    # Round the masses to the 3rd digit to ensure proper matching later
    merge_res['mod_mass'] = round(merge_res.xlinkermass.astype(float), 3)
    
    # Modify the "id" column to include the full mod_mass, not just integer values
    for index,row in merge_res.iterrows():
        xq_old_id = row['id']
        xq_mod_mass = str(row['mod_mass'])
        xq_old_id = "-".join(xq_old_id.split("-")[0:2])
        xq_new_id = xq_old_id+"-"+xq_mod_mass
        merge_res.loc[index, 'id'] = xq_new_id


    # Merge the shifts into the result dataframe to get the final df
    graph_df = pd.merge(merge_res, shifts, left_on='mod_mass', right_on='mass', how='inner')
    # Remove the column mass as it is redundant
    graph_df = graph_df.drop(columns='mass')
    graph_df.to_csv(out_file)
    print('done with consolidation')
    
if __name__ == "__main__":
    main()
