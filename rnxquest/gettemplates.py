# -*- coding: utf-8 -*-
"""
Created on 19.02.2021

Copy template files from the Python package to a working directory

v0.2: differentiates between plotting and definition files; allows direct call from command line
v0.1: Initial version. 

@author: Chris
"""

from pkg_resources import resource_string
from shutil import copyfile
import os
import sys
import stat


def main(**kwargs):
# Get the current working directory
    cwd = os.getcwd()
       
    # Set all copy flags to negative at first
    plot_copy = False
    def_copy = False
    exp_copy = False
    custloss_copy = False
    
    # Get the arguments that have been specified and set the copy flags appropriately
    if '-h' in sys.argv:
        print('A script to copy template files for xQuest searches of protein-RNA XL data. Add (only one at a time!): \n - "-def" as a parameter (i.e. "rnxq_getfiles -def") to copy definition files \n - "-plot" to copy plotting files \n - "-exp" to copy experimental plotting files \n - "-custloss" to copy an example custom loss dictionary file (for the parameter generation script)')
        sys.exit(0)
    elif '-plot' in sys.argv:
        plot_copy = True
    elif '-def' in sys.argv:
        def_copy = True
    elif '-exp' in sys.argv:
        exp_copy = True
    elif '-custloss' in sys.argv:
        custloss_copy = True
    else:
        print("Error: no valid copy type selected. No template files copied. Use \"rnxq_getfiles -h\" for help.")
        sys.exit(1)
    
    # Copying of plotting (notebook and batch) files
    if plot_copy == True:
        print("Copying plotting files")
        # Construct the template notebook path
        notebook_path = '/'.join(('templates','plot_xQuest.ipynb'))
        plot_notebook = resource_string(__name__, notebook_path)
        #print(plot_notebook)
        # Create a new file with the contents of the template file
        pynb_file = open("plot_xQuest.ipynb", "wb")
        pynb_file.write(plot_notebook)
        pynb_file.close()
        #copyfile(plot_notebook, cwd)
        
        # Construct the template batch file path
        bat_path = '/'.join(('templates','startJupyter.bat'))
        plot_bat = resource_string(__name__, bat_path)
        # Create a new file with the contents of the template file
        bat_file = open("startJupyter.bat", "wb")
        bat_file.write(plot_bat)
        bat_file.close()
        # Set permissions so all can execute batch
        os.chmod('startJupyter.bat', 0o777)
        print("Done copying plotting files")
        
    # Copying of definition files
    if def_copy == True:
        print("Copying template .def files")
        # Construct the xmm path
        xmm_path = '/'.join(('templates','RNxQ_xmm.def'))
        xmm_template = resource_string(__name__, xmm_path)
        #print(plot_notebook)
        # Create a new file with the contents of the template file
        xmm_file = open("xmm.def", "wb")
        xmm_file.write(xmm_template)
        xmm_file.close()

        # Construct the xquest path
        xqdef_path = '/'.join(('templates','RNxQ_xquest.def'))
        xqdef_template = resource_string(__name__, xqdef_path)
        #print(plot_notebook)
        # Create a new file with the contents of the template file
        xqdef_file = open("xquest.def", "wb")
        xqdef_file.write(xqdef_template)
        xqdef_file.close()
        print("Done copying .def files")
    
    # Copy the experimental plotting files
    if exp_copy == True:
        print("Copying experimental plotting files")
        # Construct the template notebook path
        notebook_path = '/'.join(('templates','plot_xQuest_experimental.ipynb'))
        plot_notebook = resource_string(__name__, notebook_path)
        #print(plot_notebook)
        # Create a new file with the contents of the template file
        pynb_file = open("plot_xQuest_experimental.ipynb", "wb")
        pynb_file.write(plot_notebook)
        pynb_file.close()
        #copyfile(plot_notebook, cwd)
        
        # Construct the template batch file path
        bat_path = '/'.join(('templates','startJupyter.bat'))
        plot_bat = resource_string(__name__, bat_path)
        # Create a new file with the contents of the template file
        bat_file = open("startJupyter.bat", "wb")
        bat_file.write(plot_bat)
        bat_file.close()
        print("Done copying plotting files")
        
    # Copy the custom loss dictionary
    if custloss_copy == True:
        print("Copying custom loss dictionary example file, \"custom_losses_example.txt\", to current directory.")
        # Construct the template notebook path
        lossdict_path = '/'.join(('templates','custom_losses_example.txt'))
        lossdict = resource_string(__name__, lossdict_path)
        # Create a new file with the contents of the template file
        losdic_file = open("custom_losses_example.txt", "wb")
        losdic_file.write(lossdict)
        losdic_file.close()

            

if __name__ == "__main__": 
    # calling the main function from the command line
    main() 
