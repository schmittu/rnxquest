#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
import math
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from scipy.optimize import curve_fit
from scipy import stats
import os
import mokapot
import rnxquest.rnxq2pin

'''
Functions required for python based analysis of xQuest results.

Chnangelog
-27.05.2022: Removes redundnat commented code from module split
-16.03.2021 - split FDR and plotting functions into different modules
-03.03.2021: Adds tidy functions to enable convenient execution of FDR analyses
-19.02.2021: Renamed main script to put into package (plottols, formerly pyxquest)

e.g.:

- Function to modify plots to mimic most of original layout from R
- Different color palettes used for graphical representatoion of cross-link nucleotide adducts 
- Available palettes:
    - color_palette_standard
    - color_palette_1
'''


            
def swap(c, i, j):
    """swaps two characters at positions i and j in String c"""
    c = list(c)
    # swap to characters as tuples
    c[i], c[j] = c[j], c[i]
    return ''.join(c)


def permute(c, left, right, permutations):
    """recursively swaps characters to generate all possible combinations"""
    if left == right:
        permutations.append(c)
    else:
        for i in range(left, right):
            c = swap(c, left, i)
            permute(c, left+1, right, permutations)
            c = swap(c, left, i)


def get_permutations(c):
    """generates all permutations of the String c and return a set of those"""
    permutations = []

    permute(c, 0, len(c), permutations)
    # by returning a set, only the unique permutations are retained
    return set(permutations)

def get_heat_map(in_df, seq, min_pos, max_pos, width=600, height=500, tickfontsize=7.5, save_name="RNA_loc_heatmap.svg", unique=False):
    #make unique if required
    if unique==True:
        in_df = in_df.sort_values('score').drop_duplicates(["id"],keep='last')
    
    # save RNA length to variable
    length_rna = len(seq)
    # read the consolidate csv file to data frame
    
    # prepare two-dimensional matrix for the heat map
    matrix = [[0 for i in range(len(seq))] for j in range(0, max_pos - min_pos + 1)]
    
    # loop over all cross-link entries and add value-information to the heat map matrix
    for index, row in in_df.iterrows():
        # only use cross-links above a certain score cut-off
        # get the position of the cross -link (AbsPos1)
        pos = row['AbsPos1']
        # get the sequence of the RNA adduct
        adduct = row['modification']
        # get all possible RNA adduct sequence permutations
        adduct_permutations = get_permutations(adduct)
        # save RNA length to variable
        length_adduct = len(adduct)
        # prepare hit-array that will hold the possible sites of cross-linking based on sequence composition
        # and cross-link adduct composition
        hits = [0 for j in range(length_rna)]
        _sum = 0
        # sliding window over the rna length with the width of length_adduct
        for j in range(length_rna - len(adduct) + 1):
            substring = seq[j:j + len(adduct)]
            if substring in adduct_permutations:
                for k in range(length_adduct):
                    # the hit position will be normalized by the number of different positions
                    # that the cross-link points to
                    _sum += 1
                    hits[j + k] += 1
        if _sum>0:
            # normalization
            for j in range(length_rna):
                hits[j] /= _sum
            # hit counts are added to the positional matrix
            for j in range(length_rna):
                try:
                    matrix[pos - min_pos][j] += hits[j]
                except IndexError:
                    "Ignore this data point, as it is out of the later viewport"
    # calculate square root of the values in each cell to reduce dynamic range in the figure
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            matrix[i][j] = math.sqrt(matrix[i][j])

    # store data into a new data frame, used for the generation of the heat map
    df = pd.DataFrame(matrix, index=range(min_pos, max_pos + 1), columns=list(seq))
    # plot using plotly
    RNA_loc = go.Figure(data=go.Heatmap(
    z=df.values.tolist(),
    x=[a + '\u200B' * i for i,a in enumerate(seq)],
    y=list(range(min_pos, max_pos + 1)),
    colorscale='Magma'),)
    
    RNA_loc.update_layout(
    autosize=True,
    xaxis = (dict(#tickmode = 'array',
	#ticktext = list(seq),
	tickangle = 0,
	tickfont = dict(size = tickfontsize),
	dtick=1)),
    width=width,
    height=height
    )
    
    RNA_loc.update_xaxes(title_text='RNA sequence')
    RNA_loc.update_yaxes(title_text='Amino acid position')
    RNA_loc.write_image(save_name)
    RNA_loc.show()


def update_figure(fig, title="", x_axis_title = 'x', y_axis_title='y', width = 900, height = 750):
    ''' a function that formats the plotly plot in a way similar to the original R-plots by Chris P. Sarnowski'''
    
    import plotly.graph_objs as go
    try: #This is here to account for the fact that new px version already tidies the legend - CS can keep new version and MG the old version
        fig.for_each_trace(lambda t: t.update(name=t.name.split('=')[1])) # remove modification = from legend entries
    except IndexError:
        pass
        #print("Legend entried already tidy!")
    
    # Remove experiment = from facet indices
    for a in fig.layout.annotations:
         a.text = "<b>" + a.text.split("=")[1] + "</b>"

    
    fig.update_xaxes(showline=True, linewidth=1, linecolor='gray', mirror=True)
    fig.update_yaxes(showline=True, linewidth=1, linecolor='gray', mirror=True)
    
    #set axis titles (required especially for facetted plots)
    for axis in fig.layout:
        if type(fig.layout[axis]) == go.layout.YAxis or type(fig.layout[axis]) == go.layout.XAxis:
            fig.layout[axis].title.text = ''


    fig.update_layout(
        # set figure size
        autosize=False,
        width=width,
        height=height,
        title=go.layout.Title(
            text=title,
            xref="paper",
            x=0,
            y=0.95,
        ),
        # keep the original annotations and add a list of new annotations for the x- and y-axis:
         annotations = list(fig.layout.annotations) +
        [go.layout.Annotation(
                x=0,
                y=0.5,
                xshift=-50,
                font=dict(
                    size=14
                ),
                showarrow=False,
                text="<b>" + y_axis_title + "</b>",
                textangle=-90,
                xref="paper",
                yref="paper"
            ),
        go.layout.Annotation(
                x=0.5,
                y=0,
                yshift=-50,
                font=dict(
                    size=14
                ),
                showarrow=False,
                text="<b>" + x_axis_title + "</b>",
                xref="paper",
                yref="paper"
            )
                 ],
        legend=go.layout.Legend(
            x=1.02,
            y=0.5,
            yanchor='middle',
            traceorder="normal",
            font=dict(
                family="sans-serif",
                size=12,
                color="black"
            )
        )
    )
    
    
def color_palette_standard():
    '''returns the default color palette for up to tetra nucleotides'''
    return {"A" : "#64eba2", "C" : "#b01003", "G" : "#924b21", "U" : "#339709", "AA" : "#1d79bb", "AC" : "#72b30f", "CA" : "#72b30f", "AG" : "#ce6031", "GA" : "#ce6031",\
"AU" : "#ac686d", "UA" : "#ac686d", "CC" : "#c9fe97", "CG" : "#59eefe", "GC" : "#59eefe", "CU" : "#267457", "UC" : "#267457", "GG" : "#bb40a6", "UG" : "#f4e60d", "GU" : "#f4e60d",\
"UU" : "#441f69", "AAA" : "#0d4b7c", "CAA" : "#bdb343", "AAC" : "#bdb343", "ACA" : "#bdb343", "GAA" : "#041a19", "AAG" : "#041a19", "AGA" : "#041a19", "UAA" : "#6f5f7d", "AAU" : "#6f5f7d",\
"AUA" : "#6f5f7d", "CCA" : "#e4023a", "ACC" : "#e4023a", "CAC" : "#e4023a", "GCA" : "#694ce5", "ACG" : "#694ce5", "CAG" : "#694ce5", "GAC" : "#694ce5", "CGA" : "#694ce5", "AGC" : "#694ce5",\
"UCA" : "#ef1e8f", "ACU" : "#ef1e8f", "CAU" : "#ef1e8f", "UAC" : "#ef1e8f", "CUA" : "#ef1e8f", "AUC" : "#ef1e8f", "GGA" : "#7c9dba", "AGG" : "#7c9dba", "GAG" : "#7c9dba", "GUA" : "#76c6f7",\
"AUG" : "#76c6f7", "GAU" : "#76c6f7", "UAG" : "#76c6f7", "UGA" : "#76c6f7", "AGU" : "#76c6f7", "UUA" : "#9effdc", "AUU" : "#9effdc", "UAU" : "#9effdc", "CCC" : "#e21bf3", "CCG" : "#0286fe",\
"GCC" : "#0286fe", "CGC" : "#0286fe", "CUC" : "#9d35ea", "CCU" : "#9d35ea", "UCC" : "#9d35ea", "CGG" : "#030db7", "GGC" : "#030db7", "GCG" : "#030db7", "CGU" : "#9ffe45", "UGC" : "#9ffe45",\
"CUG" : "#9ffe45", "GUC" : "#9ffe45", "GCU" : "#9ffe45", "UCG" : "#9ffe45", "CUU" : "#02fbff", "UUC" : "#02fbff", "UCU" : "#02fbff", "GGG" : "#f9d476", "GGU" : "#5115f8", "UGG" : "#5115f8",\
"GUG" : "#5115f8", "UGU" : "#aa1367", "GUU" : "#aa1367", "UUG" : "#aa1367", "UUU" : "#f58722", "AAAA" : "#5ef513", "AACA" : "#01befb", "CAAA" : "#01befb", "AAAC" : "#01befb", "ACAA" : "#01befb",\
"AAGA" : "#03f068", "AAAG" : "#03f068", "AGAA" : "#03f068", "GAAA" : "#03f068", "AAUA" : "#cd6cef", "AAAU" : "#cd6cef", "AUAA" : "#cd6cef", "UAAA" : "#cd6cef", "CACA" : "#1fb1c1", "ACAC" : "#1fb1c1",\
"AACC" : "#1fb1c1", "ACCA" : "#1fb1c1", "CAAC" : "#1fb1c1", "CCAA" : "#1fb1c1", "AAGC" : "#6f0378", "ACGA" : "#6f0378", "ACAG" : "#6f0378", "AGAC" : "#6f0378", "CAGA" : "#6f0378", "GACA" : "#6f0378",\
"AACG" : "#6f0378", "AGCA" : "#6f0378", "CAAG" : "#6f0378", "CGAA" : "#6f0378", "GAAC" : "#6f0378", "GCAA" : "#6f0378", "AAUC" : "#bdd5ec", "ACUA" : "#bdd5ec", "CAUA" : "#bdd5ec", "ACAU" : "#bdd5ec",\
"AUAC" : "#bdd5ec", "UAAC" : "#bdd5ec", "UCAA" : "#bdd5ec", "AACU" : "#bdd5ec", "AUCA" : "#bdd5ec", "CAAU" : "#bdd5ec", "CUAA" : "#bdd5ec", "UACA" : "#bdd5ec", "GAAG" : "#8f7df7", "GGAA" : "#8f7df7",\
"AGAG" : "#8f7df7", "GAGA" : "#8f7df7", "AAGG" : "#8f7df7", "AGGA" : "#8f7df7", "AAGU" : "#a4d608", "AUGA" : "#a4d608", "UAGA" : "#a4d608", "AGAU" : "#a4d608", "AUAG" : "#a4d608", "UAAG" : "#a4d608",\
"UGAA" : "#a4d608", "GAUA" : "#a4d608", "AAUG" : "#a4d608", "AGUA" : "#a4d608", "GAAU" : "#a4d608", "GUAA" : "#a4d608", "AUAU" : "#390e12", "UAAU" : "#390e12", "UUAA" : "#390e12", "AAUU" : "#390e12",\
"AUUA" : "#390e12", "UAUA" : "#390e12", "CACC" : "#fe5a85", "CCCA" : "#fe5a85", "ACCC" : "#fe5a85", "CCAC" : "#fe5a85", "ACGC" : "#567527", "CAGC" : "#567527", "CCGA" : "#567527", "ACCG" : "#567527",\
"AGCC" : "#567527", "CCAG" : "#567527", "CGAC" : "#567527", "GCCA" : "#567527", "GACC" : "#567527", "CACG" : "#567527", "CGCA" : "#567527", "GCAC" : "#567527", "CAUC" : "#791709", "CCUA" : "#791709",\
"UCAC" : "#791709", "ACCU" : "#791709", "AUCC" : "#791709", "CCAU" : "#791709", "CUAC" : "#791709", "UCCA" : "#791709", "UACC" : "#791709", "ACUC" : "#791709", "CACU" : "#791709", "CUCA" : "#791709",\
"CAGG" : "#f99bef", "CGGA" : "#f99bef", "GCAG" : "#f99bef", "GGAC" : "#f99bef", "AGCG" : "#f99bef", "CGAG" : "#f99bef", "GCGA" : "#f99bef", "GAGC" : "#f99bef", "ACGG" : "#f99bef", "AGGC" : "#f99bef",\
"GACG" : "#f99bef", "GGCA" : "#f99bef", "AGCU" : "#f2b72b", "AUCG" : "#f2b72b", "CGAU" : "#f2b72b", "CUAG" : "#f2b72b", "GCUA" : "#f2b72b", "GAUC" : "#f2b72b", "ACUG" : "#f2b72b", "AGUC" : "#f2b72b",\
"GACU" : "#f2b72b", "GUCA" : "#f2b72b", "ACGU" : "#f2b72b", "AUGC" : "#f2b72b", "CAUG" : "#f2b72b", "CGUA" : "#f2b72b", "UCGA" : "#f2b72b", "UAGC" : "#f2b72b", "GCAU" : "#f2b72b", "GUAC" : "#f2b72b",\
"CAGU" : "#f2b72b", "CUGA" : "#f2b72b", "UCAG" : "#f2b72b", "UGAC" : "#f2b72b", "UGCA" : "#f2b72b", "UACG" : "#f2b72b", "CAUU" : "#bfa3f8", "CUUA" : "#bfa3f8", "UCAU" : "#bfa3f8", "UUAC" : "#bfa3f8",\
"AUCU" : "#bfa3f8", "CUAU" : "#bfa3f8", "UACU" : "#bfa3f8", "UUCA" : "#bfa3f8", "ACUU" : "#bfa3f8", "AUUC" : "#bfa3f8", "UCUA" : "#bfa3f8", "UAUC" : "#bfa3f8", "GGAG" : "#035001", "AGGG" : "#035001",\
"GAGG" : "#035001", "GGGA" : "#035001", "GGAU" : "#31fb3f", "GUAG" : "#31fb3f", "AGUG" : "#31fb3f", "GAGU" : "#31fb3f", "GUGA" : "#31fb3f", "AGGU" : "#31fb3f", "AUGG" : "#31fb3f", "UGGA" : "#31fb3f",\
"UAGG" : "#31fb3f", "GAUG" : "#31fb3f", "GGUA" : "#31fb3f", "UGAG" : "#31fb3f", "UGAU" : "#474205", "UUAG" : "#474205", "GUAU" : "#474205", "AGUU" : "#474205", "AUUG" : "#474205", "UGUA" : "#474205",\
"UAUG" : "#474205", "GAUU" : "#474205", "GUUA" : "#474205", "AUGU" : "#474205", "UAGU" : "#474205", "UUGA" : "#474205", "UUAU" : "#23fec0", "AUUU" : "#23fec0", "UAUU" : "#23fec0", "UUUA" : "#23fec0",\
"CCCC" : "#26c425", "CCGC" : "#58bc65", "CCCG" : "#58bc65", "CGCC" : "#58bc65", "GCCC" : "#58bc65", "CCUC" : "#e0b6ae", "UCCC" : "#e0b6ae", "CCCU" : "#e0b6ae", "CUCC" : "#e0b6ae", "GCCG" : "#f33423",\
"GGCC" : "#f33423", "CGCG" : "#f33423", "GCGC" : "#f33423", "CCGG" : "#f33423", "CGGC" : "#f33423", "CCGU" : "#0a37f3", "CUGC" : "#0a37f3", "UCCG" : "#0a37f3", "UGCC" : "#0a37f3", "CGCU" : "#0a37f3",\
"CUCG" : "#0a37f3", "GCUC" : "#0a37f3", "CCUG" : "#0a37f3", "CGUC" : "#0a37f3", "UCGC" : "#0a37f3", "GCCU" : "#0a37f3", "GUCC" : "#0a37f3", "UCCU" : "#ad01b9", "UUCC" : "#ad01b9", "CUCU" : "#ad01b9",\
"UCUC" : "#ad01b9", "CCUU" : "#ad01b9", "CUUC" : "#ad01b9", "GGCG" : "#8805ef", "CGGG" : "#8805ef", "GCGG" : "#8805ef", "GGGC" : "#8805ef", "UGCG" : "#9a8123", "GGCU" : "#9a8123", "GUCG" : "#9a8123",\
"CGUG" : "#9a8123", "UGGC" : "#9a8123", "UCGG" : "#9a8123", "GCGU" : "#9a8123", "GUGC" : "#9a8123", "CGGU" : "#9a8123", "CUGG" : "#9a8123", "GCUG" : "#9a8123", "GGUC" : "#9a8123", "GUCU" : "#fd4edd",\
"UGCU" : "#fd4edd", "UUCG" : "#fd4edd", "UGUC" : "#fd4edd", "UCUG" : "#fd4edd", "CGUU" : "#fd4edd", "CUUG" : "#fd4edd", "UCGU" : "#fd4edd", "UUGC" : "#fd4edd", "GCUU" : "#fd4edd", "GUUC" : "#fd4edd",\
"CUGU" : "#fd4edd", "UUCU" : "#fc908f", "UCUU" : "#fc908f", "UUUC" : "#fc908f", "CUUU" : "#fc908f", "GGGG" : "#879762", "GGUG" : "#e0fe50", "UGGG" : "#e0fe50", "GGGU" : "#e0fe50", "GUGG" : "#e0fe50",\
"GGUU" : "#049a6f", "GUUG" : "#049a6f", "UGUG" : "#049a6f", "UGGU" : "#049a6f", "UUGG" : "#049a6f", "GUGU" : "#049a6f", "UGUU" : "#4c9bf1", "UUUG" : "#4c9bf1", "UUGU" : "#4c9bf1", "GUUU" : "#4c9bf1",\
"UUUU" : "#9dcd70"}
    
def color_palette_1():
    '''returns color_palette_1'''
    return {"A":"#56b1d9","C":"#ba3e75","G":"#f6cb87","U":"#1186c1","AA":"#e31a97","AC":"#a81ad2","CA":"#a81ad2","AG":"#456248","GA":"#456248",\
"AU":"#ef1207","UA":"#ef1207","CC":"#a4b554","CG":"#9371ad","GC":"#9371ad","CU":"#b052f9","UC":"#b052f9","GG":"#74ed33","UG":"#b6b7ed","GU":"#b6b7ed",\
"UU":"#8b5203","AAA":"#6d35af","CAA":"#704349","AAC":"#704349","ACA":"#704349","GAA":"#38f76b","AAG":"#38f76b","AGA":"#38f76b","UAA":"#870975","AAU":"#870975",\
"AUA":"#870975","CCA":"#c486fa","ACC":"#c486fa","CAC":"#c486fa","GCA":"#80e3f1","ACG":"#80e3f1","CAG":"#80e3f1","GAC":"#80e3f1","CGA":"#80e3f1","AGC":"#80e3f1",\
"UCA":"#f88985","ACU":"#f88985","CAU":"#f88985","UAC":"#f88985","CUA":"#f88985","AUC":"#f88985","GGA":"#1c060d","AGG":"#1c060d","GAG":"#1c060d","GUA":"#0719d0",\
"AUG":"#0719d0","GAU":"#0719d0","UAG":"#0719d0","UGA":"#0719d0","AGU":"#0719d0","UUA":"#bcfb63","AUU":"#bcfb63","UAU":"#bcfb63","CCC":"#8b91fa","CCG":"#edf7a4",\
"GCC":"#edf7a4","CGC":"#edf7a4","CUC":"#c7582e","CCU":"#c7582e","UCC":"#c7582e","CGG":"#1960fe","GGC":"#1960fe","GCG":"#1960fe","CGU":"#01f8df","UGC":"#01f8df",\
"CUG":"#01f8df","GUC":"#01f8df","GCU":"#01f8df","UCG":"#01f8df","CUU":"#e843d0","UUC":"#e843d0","UCU":"#e843d0","GGG":"#6e00c0","GGU":"#798d6b","UGG":"#798d6b",\
"GUG":"#798d6b","UGU":"#d19631","GUU":"#d19631","UUG":"#d19631","UUU":"#f3bde3","AAAA":"#01f020","AACA":"#f38700","CAAA":"#f38700","AAAC":"#f38700","ACAA":"#f38700",\
"AAGA":"#074ea7","AAAG":"#074ea7","AGAA":"#074ea7","GAAA":"#074ea7","AAUA":"#01cda5","AAAU":"#01cda5","AUAA":"#01cda5","UAAA":"#01cda5","CACA":"#f9c306","ACAC":"#f9c306",\
"AACC":"#f9c306","ACCA":"#f9c306","CAAC":"#f9c306","CCAA":"#f9c306","AAGC":"#409e4e","ACGA":"#409e4e","ACAG":"#409e4e","AGAC":"#409e4e","CAGA":"#409e4e","GACA":"#409e4e",\
"AACG":"#409e4e","AGCA":"#409e4e","CAAG":"#409e4e","CGAA":"#409e4e","GAAC":"#409e4e","GCAA":"#409e4e","AAUC":"#aee10a","ACUA":"#aee10a","CAUA":"#aee10a","ACAU":"#aee10a",\
"AUAC":"#aee10a","UAAC":"#aee10a","UCAA":"#aee10a","AACU":"#aee10a","AUCA":"#aee10a","CAAU":"#aee10a","CUAA":"#aee10a","UACA":"#aee10a","GAAG":"#939706","GGAA":"#939706",\
"AGAG":"#939706","GAGA":"#939706","AAGG":"#939706","AGGA":"#939706","AAGU":"#012b66","AUGA":"#012b66","UAGA":"#012b66","AGAU":"#012b66","AUAG":"#012b66","UAAG":"#012b66",\
"UGAA":"#012b66","GAUA":"#012b66","AAUG":"#012b66","AGUA":"#012b66","GAAU":"#012b66","GUAA":"#012b66","AUAU":"#d098ac","UAAU":"#d098ac","UUAA":"#d098ac","AAUU":"#d098ac",\
"AUUA":"#d098ac","UAUA":"#d098ac","CACC":"#21d1ff","CCCA":"#21d1ff","ACCC":"#21d1ff","CCAC":"#21d1ff","ACGC":"#62a716","CAGC":"#62a716","CCGA":"#62a716","ACCG":"#62a716",\
"AGCC":"#62a716","CCAG":"#62a716","CGAC":"#62a716","GCCA":"#62a716","GACC":"#62a716","CACG":"#62a716","CGCA":"#62a716","GCAC":"#62a716","CAUC":"#3b7d00","CCUA":"#3b7d00",\
"UCAC":"#3b7d00","ACCU":"#3b7d00","AUCC":"#3b7d00","CCAU":"#3b7d00","CUAC":"#3b7d00","UCCA":"#3b7d00","UACC":"#3b7d00","ACUC":"#3b7d00","CACU":"#3b7d00","CUCA":"#3b7d00",\
"CAGG":"#32c828","CGGA":"#32c828","GCAG":"#32c828","GGAC":"#32c828","AGCG":"#32c828","CGAG":"#32c828","GCGA":"#32c828","GAGC":"#32c828","ACGG":"#32c828","AGGC":"#32c828",\
"GACG":"#32c828","GGCA":"#32c828","AGCU":"#5f35fe","AUCG":"#5f35fe","CGAU":"#5f35fe","CUAG":"#5f35fe","GCUA":"#5f35fe","GAUC":"#5f35fe","ACUG":"#5f35fe","AGUC":"#5f35fe",\
"GACU":"#5f35fe","GUCA":"#5f35fe","ACGU":"#5f35fe","AUGC":"#5f35fe","CAUG":"#5f35fe","CGUA":"#5f35fe","UCGA":"#5f35fe","UAGC":"#5f35fe","GCAU":"#5f35fe","GUAC":"#5f35fe",\
"CAGU":"#5f35fe","CUGA":"#5f35fe","UCAG":"#5f35fe","UGAC":"#5f35fe","UGCA":"#5f35fe","UACG":"#5f35fe","CAUU":"#3d08f3","CUUA":"#3d08f3","UCAU":"#3d08f3","UUAC":"#3d08f3",\
"AUCU":"#3d08f3","CUAU":"#3d08f3","UACU":"#3d08f3","UUCA":"#3d08f3","ACUU":"#3d08f3","AUUC":"#3d08f3","UCUA":"#3d08f3","UAUC":"#3d08f3","GGAG":"#f95771","AGGG":"#f95771",\
"GAGG":"#f95771","GGGA":"#f95771","GGAU":"#f74507","GUAG":"#f74507","AGUG":"#f74507","GAGU":"#f74507","GUGA":"#f74507","AGGU":"#f74507","AUGG":"#f74507","UGGA":"#f74507",\
"UAGG":"#f74507","GAUG":"#f74507","GGUA":"#f74507","UGAG":"#f74507","UGAU":"#9f6c5a","UUAG":"#9f6c5a","GUAU":"#9f6c5a","AGUU":"#9f6c5a","AUUG":"#9f6c5a","UGUA":"#9f6c5a",\
"UAUG":"#9f6c5a","GAUU":"#9f6c5a","GUUA":"#9f6c5a","AUGU":"#9f6c5a","UAGU":"#9f6c5a","UUGA":"#9f6c5a","UUAU":"#cbfafa","AUUU":"#cbfafa","UAUU":"#cbfafa","UUUA":"#cbfafa",\
"CCCC":"#c80f3b","CCGC":"#f6fb09","CCCG":"#f6fb09","CGCC":"#f6fb09","GCCC":"#f6fb09","CCUC":"#fa83e9","UCCC":"#fa83e9","CCCU":"#fa83e9","CUCC":"#fa83e9","GCCG":"#72f196",\
"GGCC":"#72f196","CGCG":"#72f196","GCGC":"#72f196","CCGG":"#72f196","CGGC":"#72f196","CCGU":"#45dbb9","CUGC":"#45dbb9","UCCG":"#45dbb9","UGCC":"#45dbb9","CGCU":"#45dbb9",\
"CUCG":"#45dbb9","GCUC":"#45dbb9","CCUG":"#45dbb9","CGUC":"#45dbb9","UCGC":"#45dbb9","GCCU":"#45dbb9","GUCC":"#45dbb9","UCCU":"#80bc88","UUCC":"#80bc88","CUCU":"#80bc88",\
"UCUC":"#80bc88","CCUU":"#80bc88","CUUC":"#80bc88","GGCG":"#f002fd","CGGG":"#f002fd","GCGG":"#f002fd","GGGC":"#f002fd","UGCG":"#115102","GGCU":"#115102","GUCG":"#115102",\
"CGUG":"#115102","UGGC":"#115102","UCGG":"#115102","GCGU":"#115102","GUGC":"#115102","CGGU":"#115102","CUGG":"#115102","GCUG":"#115102","GGUC":"#115102","GUCU":"#3f7d92",\
"UGCU":"#3f7d92","UUCG":"#3f7d92","UGUC":"#3f7d92","UCUG":"#3f7d92","CGUU":"#3f7d92","CUUG":"#3f7d92","UCGU":"#3f7d92","UUGC":"#3f7d92","GCUU":"#3f7d92","GUUC":"#3f7d92",\
"CUGU":"#3f7d92","UUCU":"#382e47","UCUU":"#382e47","UUUC":"#382e47","CUUU":"#382e47","GGGG":"#1e036d","GGUG":"#0d774a","UGGG":"#0d774a","GGGU":"#0d774a","GUGG":"#0d774a",\
"GGUU":"#5567ed","GUUG":"#5567ed","UGUG":"#5567ed","UGGU":"#5567ed","UUGG":"#5567ed","GUGU":"#5567ed","UGUU":"#06a55a","UUUG":"#06a55a","UUGU":"#06a55a","GUUU":"#06a55a",\
"UUUU":"#5d0f02"}

# The following section provides 2 functions to help with producing a summary output table of the results

# First, define a function to split at the nth occurrence of a splitter (used for the summary table production, where spectrum is split)
# From https://stackoverflow.com/questions/27227399/python-split-a-string-at-an-underscore
def split_at(s, c, n):
    words = s.split(c)
    return c.join(words[:n]), c.join(words[n:])

# Then define the main file output function
def get_summary_csv(intab, outname="RNxQuest_output_summary.csv"):
    
    # Create a copy of the imput df
    output_df = intab

    #Create empty columns for light and heavy spectra
    output_df[['Light_spectrum','Heavy_spectrum']] = "_"

    # Split the spectrum into light and heavy, and replace mass with sequence composition in id
    for index,row in output_df.iterrows():
        #Light/Heavy spectrum stuff
        in_spec = row['spectrum']
        num_unscore = math.ceil((in_spec.count("_"))/2)
        specs = split_at(in_spec, "_", num_unscore)
        output_df.loc[index, 'Light_spectrum'] = specs[0]
        output_df.loc[index, 'Heavy_spectrum'] = specs[1]

        #id stuff
        temp_id = (split_at(row['id'], "-", 2)[0]) + "-" + row['modification']
        output_df.loc[index, 'id'] = temp_id

    # Create a subset of columns which we want to keep
    try:
        output_df = output_df[["id", 
                               "prot1", 
                               "Light_spectrum", 
                               "Heavy_spectrum", 
                               "AbsPos1", 
                               "mod_mass", 
                               "modification",
                               "loss_type",
                               "all_possible_modtypes",
                               "Mr", 
                               "Mr_precursor", 
                               "mz_precursor", 
                               "charge_precursor",
                               "error_rel",
                               "wTIC",
                               "score",
                               "FDR",
                               "FDR_type",
                               "FDR_level"]]
    except KeyError:
        print("Warning: no FDR analysis has yet been executed. Outputting results without FDR columns.")
        output_df = output_df[["id", 
                       "prot1", 
                       "Light_spectrum", 
                       "Heavy_spectrum", 
                       "AbsPos1", 
                       "mod_mass", 
                       "modification",
                       "loss_type",
                       "all_possible_modtypes",
                       "Mr", 
                       "Mr_precursor", 
                       "mz_precursor", 
                       "charge_precursor",
                       "error_rel",
                       "wTIC",
                       "score"]]

    # Rename some columns so they match MG manuscript
    output_df.rename(columns = {'id':'Id',
                                'prot1':'Protein', 
                                'mod_mass':'RNA_Mr', 
                                'modification':'RNA_composition', 
                                'loss_type':'Neutral_Loss', 
                                'all_possible_modtypes':'Isobaric_RNA_Composition', 
                                'Mr':'Mr_Calculated',
                                'Mr_precursor':'Mr_measured',
                                'mz_precursor':'Mz',
                                'charge_precursor':'z',
                                'error_rel':'Error_rel_ppm',
                                'wTIC':"TIC",
                                'score':'ld-score',
                                'FDR':'FDR_score'
                                }, inplace = True) 

    # Write to file
    output_df.to_csv(outname, index=False)
    print("Generated a summary file called "+outname)
#End of summary table functions section. 

# A function to remvoe duplicated uses of the same spectra in each data set. 
def remove_dupspec(in_df):
    
    # Create a temporary data frame to work out how many 
    # Split spectrum pair by underscore
    spec_temp_df = in_df['spectrum'].str.split("_", expand=True)

    # Measure how many elements the original spectrum filename has when split by "_"
    filename_splitlen = len(spec_temp_df.columns)

    # Attach light and heavy spectrum names to in_df
    in_df['light_spec'] = spec_temp_df.iloc[:,0:int(0.5*filename_splitlen)].astype(str).agg('_'.join, axis=1)
    in_df['heavy_spec'] = spec_temp_df.iloc[:,int(0.5*filename_splitlen):filename_splitlen].astype(str).agg('_'.join, axis=1)

    # Sort filtered_df by score descending
    in_df = in_df.sort_values(by='score', ascending=False)

    # Create dfs to hold unique scans (L and H) and filtered rows
    l_h_scans = [] #pd.DataFrame(columns=['scans'],  dtype = np.str)
    filtered_uniquescans_df = pd.DataFrame(columns=in_df.columns)

    # Check if scans are present in scans df - for every row in the filtered df...
    for index, row in in_df.iterrows():
        # If either scan is not yet present in the list of scans
        if not ((row['light_spec'] in l_h_scans) or (row['heavy_spec'] in l_h_scans)):
            # Add the light scan to the list
            l_h_scans.append(row['light_spec'])
            # Add the heavy scan to the list
            l_h_scans.append(row['heavy_spec'])
            # Copy the row to a new DF
            filtered_uniquescans_df.loc[index] = row
            #print("Light or heavy not present")
        # Otherwise, skip to next row
        else: 
            continue
            #print("Already used these scans! :(")

    # in_df.to_csv("including_dup_scans.csv")
    # Set filtered_df to contain only scans that are not reused
    print("Number of rows before filtering for reused scans: "+str(len(in_df.index)))
    in_df = filtered_uniquescans_df
    # filtered_df.to_csv("unique_scans.csv")
    print("Number of rows after filtering for reused scans: "+str(len(in_df.index)))
    global filtered_df
    filtered_df = in_df.copy()
    return filtered_df

# A function display the current working directory
def disp_wd():
    cwd = os.getcwd()
    print("Current working directory: " + cwd)
    print("Project name is: " + cwd.rsplit('\\', 1)[1])