#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
import math
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from scipy.optimize import curve_fit
from scipy import stats
import os
import mokapot
import rnxquest.rnxq2pin as rnxq2pin

'''
Functions required for python based analysis of xQuest results.

Changelog:
-14.12.2022: Adds ability to disable error plot. Renames functions. 
-27.05.2022: Removes redundnat commented code from module split
-16.03.2021 - split FDR and plotting functions into different modules
-03.03.2021: Adds tidy functions to enable convenient execution of FDR analyses
-19.02.2021: Renamed main script to put into package (plottols, formerly pyxquest)

e.g.:

- Function to modify plots to mimic most of original layout from R
- Different color palettes used for graphical representatoion of cross-link nucleotide adducts 
- Available palettes:
    - color_palette_standard
    - color_palette_1
'''

class DecoyAnalysis:
    ''' determine fdr curve for given target and decoy score lists ''' 
    
    def __init__(self, scores_target, scores_decoy):
        ''' 
        constructor:
        determine fdrcurve for quick fdr estimations at different cutoffs.
        '''
        self.scores_target = scores_target.copy()
        self.scores_decoy = scores_decoy.copy()
        self.fdrCurve = []
    
        if len(scores_decoy)>0:
            scores_target.sort(reverse=True)
            scores_decoy.sort(reverse=True)
            lastscore = 0
            lastQValue = 2
            temp_decoy = scores_decoy.copy()
            temp_target = scores_target.copy()
            
            while len(temp_decoy)>0:
                score = temp_decoy.pop()
                try:    
                    if score>lastscore or score<temp_decoy[-1]:
                        while len(temp_target)>0:
                            if temp_target[-1]<=score:
                                temp_target.pop()
                            else:
                                break
                        try:
                            qValue = len(temp_decoy)/len(temp_target)
                        except ZeroDivisionError:
                            qValue = lastQValue

                        if qValue < lastQValue:
                            try:
                                self.fdrCurve.append((self.fdrCurve[-1][0],qValue))
                            except IndexError:
                                pass
                            self.fdrCurve.append((score, qValue))
                            lastQValue = qValue
                    lastscore = score
                except IndexError:
                    qValue = lastQValue
                    if qValue < lastQValue:
                            try:
                                fdrCurve.append((fdrCurve[-1][0],qValue))
                            except IndexError:
                                pass
                            fdrCurve.append((score, qValue))
                            lastQValue = qValue
                    lastscore = score                  

    def getScore(self, fdr):
        ''' return score cutoff for a given fdr in this dataset'''
        for i, q in sorted(self.fdrCurve, key = lambda x: (-x[0], x[1])):
            if q>fdr:
                return i
        return 0
            
    def getFDR(self, score):
        ''' return false discovery rate when the given score cut off is applied to the data'''
        for i, q in self.fdrCurve:
            if i>=score:
                return q
        return 0
            
    def getXlCount(self, score):
        ''' get the number of target cross-links that are above the given score threshold. '''
        count=0
        scores_above = [a for a in self.scores_target if a >= score]
        return len(scores_above)
            
    def printFDR(self, score):
        ''' print info about the dataset at a given score cutoff'''
        print("FDR at score cutoff " + str(score) + ": " 
              + str(round(100*self.getFDR(score),1)) + "% - Hits: " + str(self.getXlCount(score)))
    
    def printScore(self, fdr):
        ''' print info about the dataset at a given fdr cutoff'''
        print("For FDR "+ f"{int(100*fdr):3d}% - score cutoff: " 
              + str(round(self.getScore(fdr),1)) + " - Hits: "+str(self.getXlCount(round(self.getScore(fdr)))))
        
    def plot(self, **kwargs):
        ''' plot target/decoy distribution and fdr-curve'''
        plot_title = kwargs.get('plot_title', None)
        save_name = kwargs.get('save_name', None)
        if len(self.scores_decoy)>0:
            fig, ax1 = plt.subplots()

            p1 = ax1.hist(self.scores_target, 30, density=False, facecolor='b', alpha=0.2, label = "target")
            p2 = ax1.hist(self.scores_decoy, 30, density=False, facecolor='r', alpha=0.2, label = "decoy")
            ax1.set_xlabel('score')
            ax1.set_ylabel('# of hits(target & decoy)')

            #set limits
            #plt.xlim([0, 50])
            #plt.ylim([0, 100])

            ax2 = ax1.twinx()
            x,y = zip(*self.fdrCurve)
            p3 = ax2.plot(x,y,'g', label='fdr-curve')
            ax2.set_ylabel('q value', color = 'g')
            ax2.tick_params(axis='y', labelcolor='g')

            #plot reference lines
            lim = ax2.get_xlim()
            plt.plot([lim[0], lim[1]], [0.01, 0.01], 'k-', lw=1,dashes=[2, 2], color='#999999')
            plt.plot([lim[0], lim[1]], [0.1, 0.1], 'k-', lw=1,dashes=[2, 2], color='#999999')
            plt.plot([lim[0], lim[1]], [0.05, 0.05], 'k-', lw=1,dashes=[2, 2], color='#999999')
            ax2.set_xlim(lim)

            lines, labels = ax1.get_legend_handles_labels()
            lines2, labels2 = ax2.get_legend_handles_labels()
            ax2.legend(lines + lines2, labels + labels2, loc=0)
            plt.title(plot_title)

            fig.tight_layout()
            # only save if there is a save_name specified
            try:
                plt.savefig(fname=save_name+".svg")
                plt.show()
                return fig, ax1, ax2
            except TypeError: 
                plt.show()
                return fig, ax1, ax2
                
                

# A simplified function to run observed FDR analysis based on the above functions
def observed_FDR(in_df, FDRcut, FDRlevel, decoy_identifier, err_plt, FDR_plot_title="", FDR_plot_save=""): #FDRtype, 
    # In the function definition, plot title and output file name are optional
    # If not specified, the plot will have no title or will not be saved, respectively. 
    
    # Set text flag for Observed FDR
    FDRtype = "Observed FDR"
    
    #Create target and decoy dfs for FDR calculation
    target_df = in_df[~in_df.prot1.str.contains(decoy_identifier)]
    decoy_df = in_df[in_df.prot1.str.contains(decoy_identifier)]
    
    #Make these unqiue if level is id
    if FDRlevel=="id":
        print("Filtering for highest scoring unique identifications for FDR calculation (ID-level)")
        target_df = target_df.sort_values('score').drop_duplicates(["id"],keep='last')
        decoy_df = decoy_df.sort_values('score').drop_duplicates(["id"],keep='last')
    elif FDRlevel=="xlsm":
        print("Calculating FDR based on redundant identifications (XL-SM level)")
        
    x_target = list(target_df['error_rel'])
    y_target = list(target_df['score'])
    #Only if required
    if err_plt==1:
        plt.plot(x_target, y_target, '.', alpha = 0.1, label = "target", color = 'b')# s d D . , o p P h H x X v + *


    x_decoy = list(decoy_df['error_rel'])
    y_decoy = list(decoy_df['score'])
    if len(x_decoy)>0:
        #Only if required
        if err_plt==1:
            plt.plot(x_decoy, y_decoy, '.', alpha = 0.1, label = "decoy", color = 'r')# s d D . , o p P h H x X v + *
            plt.xlabel('relative error')
            plt.ylabel('score')
            plt.legend()
            plt.show()

           
            
    if len(y_decoy)>0:
        ### estimate false discovery rates for certain score cutoffs
        full_decoy = DecoyAnalysis(y_target,y_decoy)
        ### plot target/decoy distribution
        full_decoy.plot(plot_title=FDR_plot_title, save_name=FDR_plot_save)

        ### print score cutoffs for different FDRs
        full_decoy.printScore(0.01)
        full_decoy.printScore(0.05)
        full_decoy.printScore(0.1)
        print()

        ### print FDRs for defined score cutoffs
        full_decoy.printFDR(10)
        full_decoy.printFDR(20)
        full_decoy.printFDR(25)
        full_decoy.printFDR(30)

    # Add columns: FDR, FDRtype, FDRlevel
    in_df['FDR'] = 0
    for index,row in in_df.iterrows():
            FDR_score = full_decoy.getFDR(row['score'])
            in_df.loc[index, 'FDR'] = FDR_score
    in_df['FDR_type'] = FDRtype
    in_df['FDR_level'] = FDRlevel
    
    # # Make filtered df available globally
    # global filtered_df
    
    # Filter in_df to the desired FDR cut-off and return
    filtered_df = in_df[in_df['FDR'] <= FDRcut]
    return filtered_df

            

def error_plot(df):
    '''plots the error distribution of xQuest results'''
    values = list(df['error_rel'])
    step = 1
    data = np.histogram(values, bins=np.arange(start = round(min(values),0)-1, stop = round(max(values),0)+1, step=1), density=True)

    # get x-values for the center of the distribution
    x = [a+step/2.0 for a in (data[1][:-1])]
    y = (data[0])

    # weighted arithmetic mean (corrected - check the section below)
    # start values:
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean)**2) / sum(y))

    def Gauss(x, a, x0, sigma):
        return a * np.exp(-(x - x0)**2 / (2 * sigma**2))

    popt,pcov = curve_fit(Gauss, x, y, p0=[max(y), mean, sigma])


    fig, ax1 = plt.subplots()

    ax1.plot(x, y, 'b+:', label='data')
    x = [min(x)+a/100*(max(x)-min(x)) for a in range(0,100)]
    ax1.plot(x, Gauss(x, *popt), 'r-', label='fit')
    ax1.legend()

    # # overlay the previous graph on top of the error graph with a secondary axis
    # ax2 = ax1.twinx() 
    # x = list(graph_df['error_rel'])
    # y = list(graph_df['score'])
    # ax2.plot(x, y, '+', alpha=0.1, label="all_data")
    
    #Create a global variable with the mean error value and sigma
    global err_mean
    global err_sigma
    err_mean = popt[1]
    err_sigma = popt[2]

    plt.title("Error distribution")
    plt.xlabel('relative error')
    plt.ylabel('frequency')
    plt.show()
    print("number of datapoints: "+ str(len(values)))
    return fig, ax1, popt, pcov
    
# A function to filter according to the error distribution properties from above
def error_filt(in_df, frac=0.995, mode="auto", lfilt=0, ufilt=0):
    if mode=="auto":
        factor = stats.norm.ppf(frac)
        keep_prop = stats.norm.ppf(frac)
        lfilt = round(err_mean - factor * err_sigma,1)
        ufilt = round(err_mean + factor * err_sigma,1)
        
        print("Keeping XLSMs with errors within the centre "+str(frac)+" of the distribution, between "+str(lfilt)+" and "+str(ufilt)+".")
        print("Entries before mass error filtering: " + str(in_df.shape[0]))
        in_df = in_df[(in_df['error_rel']>=lfilt) & (in_df['error_rel']<=ufilt)]
        print("Entries after mass error filtering: " + str(in_df.shape[0]))
        
        return in_df

    elif mode=="man":
        print("Keeping XLSMs with errors between manually defined values, "+str(lfilt)+" and "+str(ufilt)+".")
        print("Entries before mass error filtering: " + str(in_df.shape[0]))
        in_df = in_df[(in_df['error_rel']>=lfilt) & (in_df['error_rel']<=ufilt)]
        print("Entries after mass error filtering: " + str(in_df.shape[0]))
        
        return in_df
    else:
        sys.exit("Error filtering failed. Invalid input arguments for \"mode\" (must be \"man\" or \"auto\".)")


def FDR_binning(bin_type, in_df, decoy_identifier):

    if bin_type in ['small']:
        #Generate a list of modifications based on the masses of each nucleotide combination
        mods_tar_dec = list(set(list(in_df['mod_mass'])))

        #Round the list of modifications, create lists +/-0.5 Da, and then use these to define bin boundaries
        mods_tar_dec_rnd = list(set(list(np.around(np.array(mods_tar_dec),0))))
        bins_tar_dec_lo = [x - 0.5 for x in mods_tar_dec_rnd]
        bins_tar_dec_hi = [x + 0.5 for x in mods_tar_dec_rnd]
        bins_tar_dec = list(set(bins_tar_dec_lo + bins_tar_dec_hi))
        bins_tar_dec = sorted(bins_tar_dec)

        #Create new column and bin the mod mass values
        in_df['binned_mod'] = pd.cut(in_df['mod_mass'], bins_tar_dec)

    # Alternative bins for the larger mass ranges, i.e. just mono, di, tri, tetra
    elif bin_type in ['big']:
        bins_tar_dec = [0,150,450,760,1100,1400]

        #Create new column and bin the mod mass values
        in_df['binned_mod'] = pd.cut(in_df['mod_mass'], bins_tar_dec)

    # Alternative bins for the unqiue sequences detected
    elif bin_type in ['seq']:
        #Create new with bins derived by sequence type
        in_df['binned_mod'] = in_df["modification"]

    # Alternative bins for the loss types
    elif bin_type in ['loss']:
        #Create new with bins derived by loss type
        in_df['binned_mod'] = in_df["type_y"]

    else:
        print("No valid bin type selected - please check the bin variable")
        		
    #print("Binning completed using "+bin_type+" bins.")
#End of binning function


#A function to calculate the transferred FDR for each bin
#Based on the maths in paper: 10.1074/mcp.O113.030189
#Requires inputs: 
#    -in_df, which must be graph_df after running the binning script
#Generates the folliwng: 
#    -bin_score_FDR_thresholds - FDRs at every score
#    -bin_r_sq_summary - a list of r squared values for the linear models 
#    -bin_score_FDR_thresholds_summary - FDRs filtered for the specified level
def bin_FDR_calc(in_df, bin_type, FDR_cut, decoy_identifier, **kwargs):
    plotq_toggle = kwargs.get('plotq', None)
    plotprop_toggle = kwargs.get('plotprop', None)                                   
    #Partition the input df into targets and decoys
    glo_tar_df = in_df[~in_df.prot1.str.contains(decoy_identifier)]
    glo_dec_df = in_df[in_df.prot1.str.contains(decoy_identifier)]
    
    #Estimate global FDRs from the input data frames
    full_decoy = DecoyAnalysis(list(glo_tar_df['score']),list(glo_dec_df['score']))
	
	#Create a unique list of bins
    global unique_bins
    unique_bins = list(set(list(glo_tar_df['binned_mod'].astype(str)) + list(glo_dec_df['binned_mod'].astype(str))))
    
    #Start empty df in which to collect thresholds and r squared info, and make them globally available
    global bin_score_FDR_thresholds
    bin_score_FDR_thresholds  = pd.DataFrame(columns = ['bin','score', 'FDR'])
    global bin_r_sq_summary
    bin_r_sq_summary = pd.DataFrame(columns = ['bin', 'r_sq', 'bin_type'])
    
    #For each unique bin
    for mod_bin in unique_bins:
        #Create 2 new data frames, with target and decoy hits for a specific bin only
        target_df_bin = glo_tar_df[(glo_tar_df['binned_mod'].astype(str) == mod_bin)]
        decoy_df_bin = glo_dec_df[(glo_dec_df['binned_mod'].astype(str) == mod_bin)]

        #Create objects like for the global FDR, consisting of the scores for the target and decoy DBs, and create a 
        # decoy analysis object (as with global FDR above) based on these
        y_bin_target = list(target_df_bin['score'])
        y_bin_decoy = list(decoy_df_bin['score'])
        bin_decoy = DecoyAnalysis(y_bin_target,y_bin_decoy)
        #At this point, each could be plotted
        if plotq_toggle==1:
            bin_decoy.plot(plot_title=mod_bin, save_name=mod_bin+"_Bin_qval")              
        
        #For each score value in the decoy bin, calculate the number of global decoys 
        #with that score threshold and the number of bin decoys with that threshold

        #First, we should figure out how many decoys there are for this bin
        bin_decoy_len = int(len(y_bin_decoy))
   
        #There will be cases where there are no decoys - in these cases, break the loop and record it
        #These cases will later revert to global FDR
        if bin_decoy_len == 0:
            #print("No FDR for this value - not enough decoys")
            bin_score_FDR_thresholds.loc[len(bin_score_FDR_thresholds)+1] = [str(mod_bin), str(0), "N/A"]
            continue
        
        #We want to capture the part of the curve where it is linear - this corresponds to the 
        #lowest scores, so take only the lowest 80% of values  for the linear approximateion
        y_bin_decoy.sort()
        y_bin_decoy_l80 = y_bin_decoy[:round(0.8*bin_decoy_len)]

        #Create empty df for the x and y values to be solved by linear regression
        gamma_k_x  = pd.DataFrame(columns = ['score_cut','proportion_dec_bin', 'num_decoys_bin'])
        
        #For each score in the list of decoy ld.scores, calculate the proportion of global decoys (all bins)
        #for that score threshold that come from this modification set (this bin)
        for i in y_bin_decoy_l80:
            
            num_bin_decoys = (len([j for j in y_bin_decoy if j > i]))
            num_glo_decoys = (len([k for k in glo_dec_df['score'] if k > i]))
            #The try/catch below is required in case there are no global decoys at that score threshold - previously resulted in divide zero error
            try:
                prop_decoys = (len([j for j in y_bin_decoy if j > i]))/(len([k for k in glo_dec_df['score'] if k > i]))
            except ZeroDivisionError:
                prop_decoys = 0
            all_decoys_bin = len([j for j in y_bin_decoy if j > i]) #recording only, no tused to calculate
            gamma_k_x.loc[len(gamma_k_x)+1] = [i, prop_decoys, all_decoys_bin]

        #Solve equation 7 from paper using a linear regression package from scipy
        x = np.array(list(y_bin_decoy_l80)).reshape((-1, 1)) #score thresholds
        x = np.reshape(x, (len(x),)) #make it the same shape as y
        y = np.array(list(gamma_k_x['proportion_dec_bin'])) #propotion of global decoys at score threshold that come from bin
        

        #Create an instance of the class linregress, which will represent the regression model, and fit it
        bin_model_sp = stats.linregress(x,y)

        #Record the r squared value for the model
        bin_r_sq = bin_model_sp.rvalue
        bin_r_sq_summary.loc[len(bin_r_sq_summary)+1] = [mod_bin,bin_r_sq,bin_type]
        
        if plotprop_toggle==1:
            #Optional: plot how well the model fits:
            # print("The following plot relates to the bin "+mod_bin)
            #fitted_fun = lambda t: ((t*(bin_model.coef_))+bin_model.intercept_)
            fitted_fun = lambda t: ((t*(bin_model_sp.slope))+bin_model_sp.intercept)
            vfunc = np.vectorize(fitted_fun)
            y_fit = vfunc(gamma_k_x['score_cut'])
     
            fig = go.Figure()

            fig.add_trace(go.Scatter(
                x = gamma_k_x['score_cut'],
                y = gamma_k_x['proportion_dec_bin'],
                name="values"
            )),
            fig.add_trace(go.Scatter(
                x = gamma_k_x['score_cut'],
                y = y_fit,
                line_color='rgb(0,100,80)',
                name='fit',
            ))
        
            fig.update_layout(
                template="plotly_white",
                xaxis_title="ld.score cut off",
                yaxis_title="Bin proportion of global decoys",
                width = 500, 
                height= 425, 
                title={
                    'text': mod_bin,
                    'y':0.9,
                    'x':0.5,
                    'xanchor': 'center',
                    'yanchor': 'top'})

            #fig.update_traces(mode='lines')
            fig.write_image(mod_bin+'_Prop_Decs.svg')
            fig.show()
            #End of plotting
        #Now we have our linear model, we can apply it to every score in the range
        #The end goal is to figure out the FDR at each ld.score threshold in the bin
        #Once these are calculated, a final table should be written, detailing what the score cut-off 
        #is for a given FDR confidence level
        for ldscore in sorted(y_bin_decoy, reverse=True):
            #First, work out the number of target IDs greater than the score threshold, globally, and for the bin
            tot_tar_count = len(glo_tar_df[glo_tar_df['score'] > ldscore].index)
            bin_tar_count = len(target_df_bin[target_df_bin['score'] > ldscore].index)
            
            #We cannot continue if there are no targets in this bin or a divide by zero error is produced 
            if bin_tar_count==0:
                #print("Not enough bin targets at this score")
                continue
            #Calculate the proportion of targets that come from this bin at a given score
            bin_tar_proportion = bin_tar_count/tot_tar_count
            
            #Cannot continue if the proportion of targets is zero or the whole lot is multiplied by zero
            if bin_tar_proportion==0:
                #print("Proportion of global targets in this bin is zero")
                continue

            #Solve equation 8, i.e. calculate the bin FDR given the proportion of global arget hits at a score threshold x
            #that come from the bin, the linear model, and the global FDR at score x
            FDR_k_x = (len([m for m in glo_tar_df['score'] if m > ldscore])/len([n for n in y_bin_target if n > ldscore]))*(ldscore*bin_model_sp.slope + bin_model_sp.intercept)*(full_decoy.getFDR(ldscore))
            bin_score_FDR_thresholds.loc[len(bin_score_FDR_thresholds)+1] = [str(mod_bin), ldscore, FDR_k_x]

    #bin_score_FDR_thresholds.head()
    
	#Now create a summary table
	#Start an empty df in which to collect thresholds
    global bin_score_FDR_thresholds_summary
    bin_score_FDR_thresholds_summary  = pd.DataFrame(columns = ['FDR_bin','FDR_score_cut', 'FDR_level', 'FDR_type'])
    #For each unique bin
    for mod_bin in unique_bins:
        #Filter for that bin, and find the values greater than the specified FDR cut
        temp = bin_score_FDR_thresholds[(bin_score_FDR_thresholds['bin'].astype(str) == mod_bin)]
        temp = temp[(temp['FDR'] < FDR_cut)]
        #Only continue if there are values, and record in the summary table
        if temp.empty==False:
            bin_score_cut = min(temp['score'])
            bin_score_FDR_thresholds_summary.loc[len(bin_score_FDR_thresholds_summary)+1] = [mod_bin,bin_score_cut,FDR_cut, "Bin_FDR"]
        # If it is not possible to calculate a bin FDR, use the global FDR for this bin
        if temp.empty==True:
            bin_score_cut = full_decoy.getScore(FDR_cut)
            #Note the cut off for the bin in the summary table
            bin_score_FDR_thresholds_summary.loc[len(bin_score_FDR_thresholds_summary)+1] = [mod_bin,bin_score_cut,FDR_cut, "Observed_FDR_Fallback"]
    #bin_score_FDR_thresholds['FDR_bin']=bin_score_FDR_thresholds['FDR_bin'].astype(category)
    
    
#Wrap the above transferred FDR functions into an easier to access function
def tfr_FDR(work_df, FDRcut, FDRlevel, decoy_identifier, bintype):
    # Set FDR type for text labels later
    FDRtype = "Tfr_FDR"
    
    # Define input variables 
    # For binning, big (# of nts) or small (1 Da based on mod IDs) or "seq" based on unique sequences or "loss" for loss 
    # type are available
    bin_type = bintype
    
    # Check for valid bin types
    if not bin_type in ["big", "small", "seq", "loss"]:
        sys.exit("Analysis failed. Please specify a valid bin type (big, small, seq, loss).")
        
    #Make these unqiue if level is id
    if FDRlevel=="id":
        in_df = work_df.copy()    
        print("Filtering for highest scoring unique identifications for FDR calculation (ID-level)")
        in_df = in_df.sort_values('score').drop_duplicates(["id"],keep='last')
    elif FDRlevel=="xlsm":
        in_df = work_df.copy()
        print("Calculating FDR based on redundant identifications (XL-SM level)")
    
    FDR_cut = FDRcut

    # Execute binning function - requires both original and working df
    # Required inputs: bin_type, graph_df, decoy_identifier (all variables specified above) 
    print("Binning identifications using " + bin_type + " bins.")
    FDR_binning(bin_type, in_df, decoy_identifier)
    FDR_binning(bin_type, work_df, decoy_identifier)

    # Run FDR calculation function
    # Required inputs: graph_df, bin_type, FDR_cut, decoy_identifier  (all variables specified above) 
    print("Calculating FDR for each bin.")
    bin_FDR_calc(in_df, bin_type, FDR_cut, decoy_identifier)

    # Preview FDR table (created as global variable but somehow still attached to pq module)
    bin_score_FDR_thresholds.head()
    
    # Convert types before merge
    bin_score_FDR_thresholds['bin']=bin_score_FDR_thresholds['bin'].astype('str')
    in_df['binned_mod']=in_df['binned_mod'].astype('str')

    # Merge the FDR thresholds table with the graph_df table
    print("Annotating and filtering results.")
    # global filtered_df
    filtered_df = pd.merge(left = work_df, right = bin_score_FDR_thresholds_summary, left_on=['binned_mod'], right_on=['FDR_bin'])

    # Ask whether the score for this ID meets the threshold
    filtered_df['FDR_threshold_met'] = np.where(filtered_df['score'] > filtered_df['FDR_score_cut'], True, False)

    # Filter the DF for only those IDs where the threshold is met
    filtered_df = filtered_df[filtered_df['FDR_threshold_met'] == True]
    
    #Housekeeping for consistent column formats
    filtered_df['FDR_type'] = FDRtype                                     
    filtered_df['FDR_type'] = filtered_df['FDR_type'] + ", " + filtered_df['FDR_bin']
    filtered_df['FDR'] = "score_cut: " + filtered_df['FDR_score_cut'].astype(str)
    filtered_df = filtered_df.drop(columns=['binned_mod', 'FDR_threshold_met', 'FDR_bin', 'FDR_score_cut'])
    print("Transferred FDR calculations completed. Check table \"bin_score_FDR_thresholds_summary\" for summary.")
    
    # Return filtered_fd
    return filtered_df
    
# Define a function to execute mokapot analysis at psm level on results
def mokapot_FDR(in_df, FDRcut, FDRlevel, decoy_identifier): #FDRlevel, 
    print("Preparing inputs.")
    # Annotate decoys as per mokapot convention
    # Make empty column for "label" (target decoy flag)
    in_df['Label'] = 0
    
    # Set the decoy flag Label
    for index,row in in_df.iterrows():
        prot_name = row['prot1']
        if decoy_identifier in prot_name:
            in_df.loc[index, "Label"] = -1
        else:
            in_df.loc[index, "Label"] = 1
    
    # Annotate the input data frame with the same spectrum ID format as mokapot uses
    # Get scan numbers - file name MUST NOT CONTAIN periods!
    in_df['light_scan'] = in_df['spectrum'].str.split(pat=".", expand=True)[3]
    in_df['heavy_scan'] = in_df['spectrum'].str.split(pat=".", expand=True)[6]
    # Pad to 7 digits so they are always the same length
    # Note: any future script that eliminates reused scans must pad again to 14 digits before splitting!
    in_df['light_scan'] = in_df['light_scan'].str.zfill(7)
    in_df['heavy_scan'] = in_df['heavy_scan'].str.zfill(7)

    # Scan number must be an integer, but is just concatenated for simplicity
    in_df['ScanNr'] = in_df['light_scan'].astype(str) + in_df['heavy_scan'].astype(str)
    in_df['ScanNr'] = in_df['ScanNr'].astype(np.int64)

    # SpecID is a unique identifier 
    in_df['SpecId'] = in_df['id'] + "_" + in_df['ScanNr'].astype(str) + "_" + in_df['Label'].astype(str)
    
    # Write input data frame to temporary csv
    in_df.to_csv("temp_workingdf.csv")
    
    # Remove temporary columns
    in_df = in_df.drop(columns=['Label', 'ScanNr'])
    
    #Convert for mokapot
    print("Formatting identifications for mokapot.")
    rnxq2pin.main(['--infile','temp_workingdf.csv','--outfile','tempmokapotout.pin','--decoyflag',decoy_identifier])
    # Delete temporary csv after conversion
    os.remove("temp_workingdf.csv")
    
    #Execute mokapot
    print("Executing mokapot analysis.")
    
    #Read psms
    psms = mokapot.read_pin("tempmokapotout.pin")
    
    # Delete temporary PIN after conversion
    os.remove("tempmokapotout.pin")
    
    # Analyse
    global results
    global models
    results, models = mokapot.brew(psms)
 
    #Merge mokapot outputs with original PSM table
    # global output_df
    
    if FDRlevel=="xlsm":
        output_df = pd.merge(left = in_df, right = results.psms, on=['SpecId'])
        output_df['FDR_level'] = "mokapot_xlsm"
    elif FDRlevel=="id":
        output_df = pd.merge(left = in_df, right = results.peptides, on=['SpecId'])
        print("Merging with results at ID level - only the highest scoring identification of each type will be annotated with a mokapot q-value.")
        output_df['FDR_level'] = "mokapot_id"
    
    # Tidy columns to keep output consistent
    output_df = output_df.drop(columns=['Peptide', 'Proteins', 'mokapot score', 'mokapot PEP', 'light_scan', 'heavy_scan', 'SpecId', 'Label', 'ScanNr',])
    # Remove existing columns called FDR, then rename mokapot q value to FDR
    output_df = output_df.drop(columns=['FDR'])
    output_df.rename(columns = {'mokapot q-value':'FDR'
                                }, inplace = True) 
    output_df['FDR_type'] = "mokapot_qval"
    
    # Dev - print columns
    output_df.head()
    
    # Filter according to desired cut-off
    output_df = output_df[output_df['FDR'] < FDRcut]
    
    # Return output_df
    return output_df