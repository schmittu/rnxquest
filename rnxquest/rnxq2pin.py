# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 19:04:56 2021

To turn an extracted.xQuest.csv into a PIN for mokapot and Percolator.

v0.22: gets rid of dependency on FASTA file (requires later version of extraction script)
v0.21: defines as a command line callable function for packaging. Renamed to rnxq2pin
v0.2: Adds lines that lead to a column called "rel_pos" - this is a number between 0 and 1 that describes how far along the peptide the RNA is located, i.e. 0 is far NTD, 1 is far CTD
v0.1: Initial version. 

@author: Chris Sarnowski
"""

import pandas as pd
import numpy as np
# import os
# import fastaparser
import argparse
import re

    
def xqtopin(infile, outfile, decoyflag):

    ### Set working directory 
    #os.chdir("Z:/2020_0923_FDR_Work_Sandbox/Py_Analysis/")

    ### Define a decoy flag - default should be decoy_, used to set target/decoy to -1/+1
    decoy_flag = str(decoyflag)
    # Define file in and out names
    inname = str(infile)
    outname = str(outfile)
    # fasta_name = str(args.fasta)

    ### load FASTA file - will need this so that the prior/post amino acid positions can be concatenated the peptide
    ### first collect data in a list, then convert it to a data frame
    # fasta_data = []

    print("Reading input files")
    #Below lines can be useful for troubleshooting
    # with open(fasta_name) as fasta_file:
            # parser = fastaparser.Reader(fasta_file)
            # for seq in parser:
                # seq is a FastaSequence object
                # temp_id = seq.id
                # print('ID:', seq.id)
                # print('Description:', seq.description)
                # print('Sequence:', seq.sequence_as_string())
                # temp_seq = seq.sequence_as_string()
                # print()
                # fasta_data.append([temp_id, temp_seq])
    # fasta_df = pd.DataFrame(fasta_data, columns=['prot1', "full_AA_sequence"])

    ### load output file as pandas data frame
    extract_df = pd.read_csv(inname)
    extract_subset_df = extract_df[["spectrum", "Mr", "Mr_precursor", "apriori_pmatch_common", "apriori_pmatch_xlink", "wTIC",	"AbsPos1",	"AA", "topology", "mod_mass", "Search", "xcorrb",	"xcorrx",	"match_odds", "charge", "intsum", "unmod_pep", "pep_start", "pep_end", "aa_bef", "aa_aft", "error_rel" , "error",  "prot1", "id", "modification"]] #"Peptide",
    extract_subset_df = extract_subset_df.copy()

    # Join the fasta DF to the extracted xquest df
    # extract_subset_df = pd.merge(extract_subset_df, fasta_df, how="left", on=["prot1"])

    print("Calculating and converting!")
    # Make empty df columns dynamically according to number of charges
    # for z in range(1, max(extract_subset_df['charge'])+1):
    #     extract_subset_df["charge_"+str(z)] = 0
        
    # Get the length of the RNA
    extract_subset_df['RNA_len'] = extract_subset_df['modification'].str.len()
    # Make empty df columns dynamically according to number of RNA lengths
    for a in range(1, max(extract_subset_df['RNA_len'])+1):
        extract_subset_df["RNA_"+str(a)] = 0

    # Get the shift from the search
    extract_subset_df["Search"] = extract_subset_df["Search"].str.rsplit(n=2, pat="_", expand=True).iloc[:,-2]+"."+extract_subset_df["Search"].str.rsplit(n=2, pat="_", expand=True).iloc[:,-1]
        
    # Make empty column for "label" (target decoy flag)
    extract_subset_df['Label'] = 0

    # Make empty column for "rel_pos" i.e. the distance along the peptide, NTD to CTD as a proportion of number of AAs
    extract_subset_df['rel_pos'] = 0

    # Set an array to encode the amino acid position numbers
    AA_positions = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    # Make a peptide column in the same format as the PIN requires
    extract_subset_df['Peptide'] = ""
    for index,row in extract_subset_df.iterrows():
        
        #Complicated way to get unmodificed peptide with previous and next AAs
        start_num = row['pep_start']
        # start_min1 = row['pep_start']-1
        end_num = row['pep_end']
        # end_plu1 = row['pep_end']+1
        peplen = (end_num - start_num) + 1
        peppos = int(re.findall("\d+", row['topology'])[0])
        # print(peppos)
        NTD_CTD = peppos/peplen
        try:
            # print(start_min1)
            # print(row['full_AA_sequence'])
            prev_aa = row['aa_bef']
        except IndexError:
            prev_aa = "_"
        try:
            next_aa = row['aa_aft']
        except IndexError:
            next_aa = "_"
        pepseq = row['unmod_pep']
        # pep_finform = prev_aa+"."+pepseq+"."+next_aa
        # extract_subset_df.loc[index, 'Peptide'] = pep_finform
        
        #Simpler way - use "ID" column
        extract_subset_df.loc[index, 'Peptide'] = row['id']
        
        #Set the proportion along the peptide (i.e. NTD vs CTD)
        extract_subset_df.loc[index, 'rel_pos'] = NTD_CTD
        
        #Also set the correct charge column
        # charge_exp = row['charge']
        # extract_subset_df.loc[index, "charge_"+str(charge_exp)] = 1
        
        #Also set the correct RNA_len column
        RNA_length = row['RNA_len']
        extract_subset_df.loc[index, "RNA_"+str(RNA_length)] = 1
        
        # Set the decoy flag Label
        prot_name = row['prot1']
        if decoy_flag in prot_name:
            extract_subset_df.loc[index, "Label"] = -1
        else:
            extract_subset_df.loc[index, "Label"] = 1
            
        #Encode the amino acid position as a number
        XL_AA = row['AA']
        XL_AA = AA_positions.find(XL_AA)
        extract_subset_df.loc[index, "AA"] = int(XL_AA)+1


    # Get scan numbers - file name MUST NOT CONTAIN periods!
    extract_subset_df['light_scan'] = extract_subset_df['spectrum'].str.split(pat=".", expand=True)[3]
    extract_subset_df['heavy_scan'] = extract_subset_df['spectrum'].str.split(pat=".", expand=True)[6]
    # Pad to 7 digits so they are always the same length
    # Note: any future script that eliminates reused scans must pad again to 14 digits before splitting!
    extract_subset_df['light_scan'] = extract_subset_df['light_scan'].str.zfill(7)
    extract_subset_df['heavy_scan'] = extract_subset_df['heavy_scan'].str.zfill(7)

    # Scan number must be an integer, but is just concatenated for simplicity
    extract_subset_df['ScanNr'] = extract_subset_df['light_scan'].astype(str) + extract_subset_df['heavy_scan'].astype(str)
    extract_subset_df['ScanNr'] = extract_subset_df['ScanNr'].astype(np.int64)


    # SpecID is a unique identifier 
    extract_subset_df['SpecId'] = extract_subset_df['id'] + "_" + extract_subset_df['ScanNr'].astype(str) + "_" + extract_subset_df['Label'].astype(str)

    print("Formatting output file")

    # Rename the proteins column
    extract_subset_df = extract_subset_df.rename(columns={"prot1": "Proteins"})


    # Get the full list of column names
    col_names = extract_subset_df.columns.tolist()

    # Drop the values that are not needed, AND the names that need to be ion specific places (added back manually later)
    col_names.remove("id")
    col_names.remove("topology")
    col_names.remove("RNA_len")
    col_names.remove("modification")
    col_names.remove("charge")
    col_names.remove("pep_start")
    col_names.remove("pep_end")
    col_names.remove("unmod_pep")
    col_names.remove("aa_bef")
    col_names.remove("aa_aft")
    col_names.remove("light_scan")
    col_names.remove("heavy_scan")
    col_names.remove("spectrum")
    col_names.remove("Label")
    col_names.remove("SpecId")
    col_names.remove("ScanNr")
    col_names.remove("Peptide")
    col_names.remove("Proteins")
    # Add the removed but necessary names back in the right positions
    col_names_output = ["SpecId", "Label", "ScanNr"] + col_names + ["Peptide", "Proteins"]

    # Put the columns in the new data frame
    pin_df = extract_subset_df.copy()
    pin_df = pin_df[col_names_output]

    # Set types
    pin_df['AA'] = pin_df['AA'].astype(np.int64)
    pin_df['Search'] = pin_df['Search'].astype(np.float64)
    # write to tsv
    print("Writing output file")
    pin_df.to_csv(outname, sep="\t", index=False)
    print("Done!")

# 2 versions of the execution of the above function are implemented, due to differences in how arguments are parsed
# The first deals with how the arguments are parsed whent he script is called by another module
# The second is specific to the case where the script is called fromt he command line
# More details (top 2 answers): https://stackoverflow.com/questions/14500183/in-python-can-i-call-the-main-of-an-imported-module

def main(args):
    # Get arguments from command line
    print("Setting variables from the command line")
    parser = argparse.ArgumentParser(
        description='A script to convert extracted.xQuest.csv files to Percolator/mokapot PIN files. Inputs required: xQuest extracted csv, fasta, decoy flag, (optional) filenames; outputs PIN file')
    parser.add_argument('-i', '--infile', required=False, default="extracted.xQuest.csv",
                        help='Input csv file name. Default: extracted.xQuest.csv')
    parser.add_argument('-o', '--outfile', required=False, default="extracted_xQuest.pin",
                        help='Output PIN file name. Default: extracted_xQuest.pin')  
    parser.add_argument('-d', '--decoyflag', required=False, default="decoy_",
                        help='The decoy identifier string - every decoy database entry should start with this')
        # Make argument parsing compatible with command line and as part of FDR function
    args = parser.parse_args(args)
    xqtopin(args.infile, args.outfile, args.decoyflag)
    
    
def cmd_ln_main():
    # Get arguments from command line
    print("Setting variables from the command line")
    parser = argparse.ArgumentParser(
        description='A script to convert extracted.xQuest.csv files to Percolator/mokapot PIN files. Inputs required: xQuest extracted csv, fasta, decoy flag, (optional) filenames; outputs PIN file')
    parser.add_argument('-i', '--infile', required=False, default="extracted.xQuest.csv",
                        help='Input csv file name. Default: extracted.xQuest.csv')
    parser.add_argument('-o', '--outfile', required=False, default="extracted_xQuest.pin",
                        help='Output PIN file name. Default: extracted_xQuest.pin')  
    parser.add_argument('-d', '--decoyflag', required=False, default="decoy_",
                        help='The decoy identifier string - every decoy database entry should start with this')
        # Make argument parsing compatible with command line and as part of FDR function
    args = parser.parse_args()
    xqtopin(args.infile, args.outfile, args.decoyflag)

# if __name__ == "__main__": 
    # calling the main function 
    # main() 
if __name__ == '__main__':
    import sys
    main(sys.argv[1:])