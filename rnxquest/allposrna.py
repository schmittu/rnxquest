# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 08:29:28 2021
Based on:
    https://stackoverflow.com/questions/12180953/all-triplet-combinations-6-values-at-a-time
    
Changelog:
19.02.2021
-Sets up script in the format for a python package
@author: Chris
"""
import argparse
import itertools as itt #import izip_longest, islice, combinations

def main():
    parser = argparse.ArgumentParser(
        description='A script to generate an RNA sequence for the RNxQuest pipeline that contains all possible compositions of nucleotides up to a given length')
    parser.add_argument('-m', '--maxlen', required=False, default=4,
                        help='Maximum length of RNA attachment, i.e. 2 will give a sequence that contains all possible mono and dinucleotide compositions.')
    args = parser.parse_args()

    max_len = int(args.maxlen)


    all_combs = []

    for comb in itt.combinations(max_len*["U","G","C","A"],max_len):
        all_combs.append("".join(sorted(comb)))
        # print(triplet)

    all_combs_set = "".join(sorted(set(all_combs)))
    print("A sequence that gives all possible combinations of length "+str(max_len)+" is: "+all_combs_set)

if __name__ == "__main__": 
    # calling the main function 
    main() 